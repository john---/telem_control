package TelemControl::Model::Sensors::Weather;
use parent TelemControl::Model::Sensors::Base;

use strict;
use warnings;

#use DarkSky::API;
use Weather::OpenWeatherMap;

use Mojo::JSON qw(decode_json);

use Data::Dumper;

sub init {
    my $self = shift;

    $self->log->debug( sprintf( 'initialize %s', $self->node->{name} ) );

    # items spoken but not stored
    $self->node->{summary} = '';
    $self->node->{wind_speed} = -10;

    $self->pg->pubsub->listen(location_msg => sub {
	my ($pubsub, $payload) = @_;
	my $loc = decode_json($payload);
	$self->node->{device}{lat} = $loc->{lat} || $self->node->{device}{lat};
	$self->node->{device}{lon} = $loc->{lon} || $self->node->{device}{lon};
	$self->log->debug(sprintf('lat: %f lon: %f', $self->node->{device}{lat}, $self->node->{device}{lon}));
    });

    $self->node->{weather_getter} = Weather::OpenWeatherMap->new(
        api_key => $self->node->{device}{key}
    );
    
    $self->SUPER::init();

    return $self;
}

sub _read {
    my $self = shift;

    my $current = $self->node->{weather_getter}->get_weather(
        location => sprintf('lat %f, long %f', $self->node->{device}{lat}, $self->node->{device}{lon}),
    );
    #$self->log->debug('got current weather' . Dumper($current));

    # NOTE: Hourly forecast can be looked at to get upcoming changes.
    #       Overall it does not look very granular (e.g. rain in 5 minutes)
    # my $forecast = $self->node->{weather_getter}->get_weather(
    #     location => sprintf('lat %f, long %f', $self->node->{device}{lat}, $self->node->{device}{lon}),
    #     forecast => 1,
    #     hourly => 1,
    # );
    # $self->log->debug('got forecast' . Dumper($forecast));
    # conditions_verbose

    # look for interesting things to report on
    # if (exists $forecast->{alerts}) {
    # 	#$self->log->debug('got an alert' . Dumper($forecast->{alerts}));
    # 	foreach my $alert (@{$forecast->{alerts}}) {
    # 	    $self->log->debug(sprintf('alert: %s', $alert->{title}));
    # 	}
    # }

    if ($self->node->{summary} ne $current->conditions_verbose) {
        $self->log->debug(sprintf('weather was "%s" and now is "%s"', $self->node->{summary}, $current->conditions_verbose));
        $self->node->{summary} = $current->conditions_verbose;
        $self->speak( sprintf('Weather is %s', $self->node->{summary}) );
    }

    if ( abs($self->node->{wind_speed} - $current->wind_speed_mph) > 3 ) {
        $self->node->{wind_speed} = $current->wind_speed_mph;
        $self->speak( sprintf('Wind speed is %.1f miles per hour', $self->node->{wind_speed}) );
    }

    $self->node->{raw} = $current->temp_f;
    return $self->node->{raw};
}

1;
