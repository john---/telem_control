requires 'Mojo::IOLoop';
requires 'Mojo::IOLoop::Subprocess';
requires 'Mojo::JSON';
requires 'Mojo::Pg';
requires 'Mojo::Log';
requires 'FindBin';
requires 'File::Basename';
requires 'Exporter::Tiny', '>= 1.004001';
requires 'File::Copy::Recursive';
requires 'Weather::OpenWeatherMap';

requires 'Device::Chip::Adapter';
requires 'Device::Chip::Adapter::LinuxKernel';
requires 'Device::Chip::INA219';
requires 'Device::Chip::TMP102';
requires 'Device::Chip::ADT7470';
